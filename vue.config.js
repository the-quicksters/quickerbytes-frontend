var path = require('path');
var merge = require('webpack-merge')

module.exports = {
  chainWebpack: (config) => {
    config.module
    .rule('scss')
    .use('sass-loader')
    .tap(options =>
        merge(options, {
            includePaths: [path.resolve(__dirname, 'node_modules')],
        })
    )
  },
  pwa: {
    themeColor: '#000000',
    workboxOptions: {
      importScripts: ['push-sw.js']
    }
  }
}