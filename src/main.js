import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import cognitoAuth from './cognito'
import credentialManager from './credentials'
import VueMDCAdapter from 'vue-mdc-adapter'
import axios from 'axios'
import VueAxios from 'vue-axios'
import './main.scss'
import './registerServiceWorker'

Vue.use(VueMDCAdapter)
Vue.use(VueAxios, axios)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  cognitoAuth,
  credentialManager,
  render: h => h(App)
}).$mount('#app')
