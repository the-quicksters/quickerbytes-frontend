export default class CredentialManager {
    constructor() {
    }

    configure(config) {
        this.options = config
    }

    requestCredentials(mediation, cb) {
        if(window.PasswordCredential) {
            navigator.credentials.get({password: true, mediation}).then(credentials => {
                if(cb)
                    cb(null, credentials)
            }).catch(err => {
                if(cb)
                    cb(err, null)
            })
        } else {
            if(cb)
                cb('Credential Management API not supported', null)
        }
    }

    storeCredentials(email, password, cb) {
        if(window.PasswordCredential) {
            let credential = new PasswordCredential({
                id: email,
                password,
                name: email
            })
            navigator.credentials.store(credential).then(credentials => {
                if(cb)
                    cb(null, null)
            }).catch(err => {
                if(cb)
                    cb(err, null)
            })
        } else {
            console.log('Not soring credentials')
            if(cb)
                cb('Credential Management API not supported', null)
        }
    }

    preventSilentAccess(email, password, cb) {
        if(window.PasswordCredential) {
            navigator.credentials.preventSilentAccess()
        } else {
            if(cb)
                cb('Credential Management API not supported', null)
        }
    }

    onChange() {}
}

CredentialManager.install = function(Vue, options) {
    Object.defineProperty(Vue.prototype, '$credentialManager', {
        get() {return this.$root._credentialManager}
    })

    Vue.mixin({
        beforeCreate() {
            if(this.$options.credentialManager) {
                this._credentialManager = this.$options.credentialManager
                this._credentialManager.configure(options)
            }
        }
    })
}