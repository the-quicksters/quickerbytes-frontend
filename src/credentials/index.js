import Vue from 'vue'
import CredentialManager from './credentials'

Vue.use(CredentialManager, {})

export default new CredentialManager()