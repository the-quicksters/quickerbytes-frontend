import Vue from 'vue'
import Vuex from 'vuex'
import Fuse from 'fuse.js'
import cognitoAuth from './cognito/';
import { CognitoIdentityServiceProvider } from 'aws-sdk';

Vue.use(Vuex)

let defaultFood = [
  {
    id: 'mcchicken',
    name: 'McChicken',
    description: 'A sandwich with chicken',
    restaurant: 'mcdonalds',
    price: 1.09
  },
  {
    id: 'mfries',
    name: 'French Fries',
    description: 'Square tubes of potato',
    restaurant: 'mcdonalds',
    price: 14.99
  },
  {
    id: 'mcnuggets',
    name: 'McNuggets',
    description: 'Nuggets made of chicken',
    restaurant: 'mcdonalds',
    price: 0.12
  },
  {
    id: 'salad',
    name: 'Salad',
    description: 'Food made from dead plants',
    restaurant: 'mcdonalds',
    price: 9.13
  },
  {
    id: 'iscream',
    name: 'Ice Cream',
    description: 'I scream, you scream, we all scream for ice cream',
    restaurant: 'mcdonalds',
    price: 28.34
  },
  {
    id: 'applepie',
    name: 'Apple Pie',
    description: 'A McDonalds apple pie',
    restaurant: 'mcdonalds',
    price: 8.99
  }
]

let fuseOptions = {
  keys: ['name', 'description', 'restaurant'],
  id: 'id'
}

let fuse = new Fuse(defaultFood, fuseOptions)

export default new Vuex.Store({
  state: {
    cart: ['salad', 'mfries'],
    query: '',
    search: [],
    foodOptions: defaultFood,
    userInfo: {

    }
  },
  getters: {
    cartItems(state) {
      return state.cart.map(item => state.foodOptions.filter(food => food.id === item)[0])
    },
    searchItems(state) {
      return state.search.map(item => state.foodOptions.filter(food => food.id === item)[0])
    },
    setUserInfo() {

    }
  },
  mutations: {
    search(state, query) {
      state.query = query
      state.search = fuse.search(state.query)
    },
    updateFoodOptions(state, foodOptions) {
      state.foodOptions = foodOptions
      fuse.setCollection(foodOptions)
    }
  },
  actions: {
    getUserInfo() {
      //cognitoAuth.getCurrentUser().getUserData()
    }
  }
})
