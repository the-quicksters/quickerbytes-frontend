import AWS, { Config, CognitoIdentityCredentials } from "aws-sdk";
import axios from "axios";
import { CognitoUserPool, CognitoUserAttribute, AuthenticationDetails, CognitoUser } from "amazon-cognito-identity-js";
import { debug } from "util";

export default class CognitoAuth {
    constructor() {
        this.userSession = null
    }

    configure(config) {
        console.log('CONFIG')
        console.log(config.UserPoolId)
        console.log(config.ClientId)
        this.userPool = new CognitoUserPool({
            UserPoolId: config.UserPoolId,
            ClientId: config.ClientId
        })

        Config.region = config.region
        Config.credentials = new CognitoIdentityCredentials({
            IdentityPoolId: config.IdentityPoolId
        })

        this.options = config
    }

    isAuthenticated(cb) {
        let cognitoUser = this.getCurrentUser()
        if(cognitoUser != null) {
            cognitoUser.getSession((err, session) => {
                if(err) {
                    return cb(err, false)
                } else {
                    return cb(null, true)
                }
            })
        } else {
            return cb(null, false)
        }
    }

    signIn(username, pass, cb) {
        let authenticationDetails = new AuthenticationDetails({
            Username: username,
            Password: pass
        })

        console.log('USER POOL: ')
        console.log(this.userPool)
        let cognitoUser = new CognitoUser({
            Username: username,
            Pool: this.userPool
        })
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: (async res => {
                var logins = {}
                logins[`cognito-idp.${this.options.region}.amazonaws.com/${this.options.UserPoolId}`] = res.getIdToken().getJwtToken()
                
                Config.credentials = new CognitoIdentityCredentials({
                    IdentityPoolId: this.options.IdentityPoolId,
                    Logins: logins
                }, {
                    region: 'us-east-1'
                })
                
                Config.credentials.get((err) => {
                    console.log(err)
                    console.log('Identity ID: ' + Config.credentials.identityId)
                    console.log('Access Key ID: ' + Config.credentials.accessKeyId)
                    console.log('Secret Access Key: ' + Config.credentials.secretAccessKey)
                    console.log('Session Token: ' + Config.credentials.sessionToken)

                    AWS.config.update({
                        region: 'us-east-1',
                        accessKeyId: Config.credentials.accessKeyId,
                        secretAccessKey: Config.credentials.secretAccessKey,
                        sessionToken: Config.credentials.sessionToken
                    })
    
                    console.log('Invoking helloWorld')
                    axios({
                        method: 'get',
                        url: 'https://1lsxs7qe0c.execute-api.us-east-1.amazonaws.com/dev/helloWorld',
                        headers: {
                            'Authorization': res.idToken.jwtToken
                        }
                    }).then(response => {
                        console.log('Got API Gateway response')
                        console.log(response)
                    })
                    console.log('Signed in successfully')    
                })

                this.onChange(true)

                cb(null, res)
            }),    
            onFailure: (async err => {
                console.log(err)
                cognitoUser.getSession(async (sessErr, session) => {
                    if(sessErr) {
                        cb(err, null)
                    } else {
                        var logins = {}
                        logins[`cognito-idp.${this.options.region}.amazonaws.com/${this.options.UserPoolId}`] = session.getIdToken().getJwtToken()

                        Config.credentials = new CognitoIdentityCredentials({
                            IdentityPoolId: this.options.IdentityPoolId,
                            Logins: logins
                        }, {
                            region: 'us-east-1'
                        })
                        
                        console.log(Config.credentials)
                        Config.credentials.get((err) => {
                            console.log(err)
                            console.log('Identity ID: ' + Config.credentials.identityId)
                            console.log('Access Key ID: ' + Config.credentials.accessKeyId)
                            console.log('Secret Access Key: ' + Config.credentials.secretAccessKey)
                            console.log('Session Token: ' + Config.credentials.sessionToken)
        
                            AWS.config.update({
                                accessKeyId: Config.credentials.accessKeyId,
                                secretAccessKey: Config.credentials.secretAccessKey,
                                sessionToken: Config.credentials.sessionToken
                            })
            
                            console.log('Invoking helloWorld')

                            axios({
                                method: 'get',
                                url: 'https://1lsxs7qe0c.execute-api.us-east-1.amazonaws.com/dev/helloWorld',
                                headers: {
                                    'Authorization': res.idToken.jwtToken
                                }
                            }).then(response => {
                                console.log('Got API Gateway response')
                                console.log(response)
                            })

                            console.log('Signed in successfully')            
                        })
                                
                        console.log('(Hopefully) recovered sign in')

                        this.onChange(true)

                        cb(null, session)
                    }
                })
            }),
            newPasswordRequired: (userAttribs, requiredAttribs) => {
                callback('User needs to set a password.', null)
            },
            mfaRequired: function(codeDeliveryDetails) {
                cognitoUser.sendMFACode(mfaCode, this)
            }
        })
    }

    signOut() {
        this.getCurrentUser().signOut()

        this.onChange(false)
    }

    register(username, pass, cb) {
        let attribs = [
            new CognitoUserAttribute({
                Name: 'email',
                Value: username
            })
        ]

        this.userPool.signUp(username, pass, attribs, null, cb)
    }

    confirmRegistration(username, code, cb) {
        let cognitoUser = new CognitoUser({
            Username: username,
            Pool: this.userPool
        })
        cognitoUser.confirmRegistration(code, true, cb)
    }

    resendCode(username, cb) {
        let cognitoUser = new CognitoUser({
            Username: username,
            Pool: this.userPool
        })
        cognitoUser.resendConfirmationCode(cb)
    }

    getCurrentUser () {
        return this.currentUser || (this.currentUser = this.userPool.getCurrentUser())
    }

    onChange() {}
}

CognitoAuth.install = function(Vue, options) {
    Object.defineProperty(Vue.prototype, '$cognitoAuth', {
        get() {return this.$root._cognitoAuth}
    })

    Vue.mixin({
        beforeCreate() {
            if(this.$options.cognitoAuth) {
                this._cognitoAuth = this.$options.cognitoAuth
                this._cognitoAuth.configure(options)
            }
        }
    })
}