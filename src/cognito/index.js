import Vue from 'vue'
import CognitoAuth from './cognito'

Vue.use(CognitoAuth, {
    region: 'us-east-1',
    UserPoolId: 'us-east-1_Zd2rjxnX9',
    IdentityPoolId: 'us-east-1:885d68d9-f7b2-4f63-9ec4-1a6e07a8d7b7',
    ClientId: '1so6m71ukqgp3uuj20qj2v3p0m'
})

export default new CognitoAuth()