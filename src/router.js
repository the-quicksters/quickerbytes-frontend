import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import ConfirmRegistration from './views/ConfirmRegistration.vue'
import Zestimate from './views/Zestimate.vue'
import cognitoAuth from '@/cognito'
import credentialManager from '@/credentials'

Vue.use(Router)

function authRequired(to, from, next) {
  cognitoAuth.isAuthenticated((err, authenticated) => {
    if(authenticated) {
      next()
    } else {
      next({
        path: '/login',
        query: {
          redirect: to.path
        }
      })
    }
  })
}

function signOut(to, from, next) {
  cognitoAuth.signOut()
  credentialManager.preventSilentAccess()
  next('/')
}

export default new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login,
      beforeEnter(to, from, next) {
        cognitoAuth.isAuthenticated((err, authenticated) => {
          if(authenticated) {
            next({
              path: to.query.redirect || '/dashboard'
            })
          } else {
            next()
          }
        })
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/confirm',
      name: 'confirmregister',
      component: ConfirmRegistration
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      beforeEnter: authRequired
    },
    {
      path: '/zestimate',
      name: 'getzestimate',
      component: Zestimate,
      beforeEnter: authRequired
    },
    {
      path: '/signout',
      beforeEnter: signOut
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
